#!/usr/bin/env python3

from Report import Report
rpt = Report()

## TODO: Add your code here

# Our imports
import sys
import os

# Hard coding of our file names
csvFileHardCode = "2017.annual.singlefile.csv"
csvTitleHardCode = "area_titles.csv"


# function definition for creating our new dataset based on industry and own code and throwing away duplicated data
# This function returns a list
def new_dataset(industry_code, own_code):
    new_set = []
    with open(csvFileHardCode) as f:
        for line in f:
            line = line.rstrip().replace('"', '').split(',')
            if all(letter not in line[0] for letter in ['C', 'US', 'M']) and '000' not in line[0][-3:] and own_code == line[1] and industry_code == line[2]:
                line[10], line[8], line[9] = int(line[10]), int(line[8]), int(line[9])
                new_set.append(line)
    return new_set


# function definition for finding the totals of a focus set we want to add up in a dataset
# This function returns the value of total
# NOTE: this function will crash if focus parameter is not one of the specified strings
def calc_totals(focus, list_in):
    total = 0
    if focus == 'wages':
        param = 10
    elif focus == 'estabs':
        param = 8
    elif focus == 'emplvl':
        param = 9
    for line in list_in:
        total += int(line[param])
    return total


# function definition for finding the max value of a focus set and looking up the name of the fips
# this function returns the name as a string and the max_value
# NOTE: this function will crash if focus parameter is not one of the specified strings
def calc_maxes(focus, list_in):
    current_max = 0
    fips = ''
    if focus == 'wages':
        param = 10
    elif focus == 'estabs':
        param = 8
    elif focus == 'emplvl':
        param = 9
    for line in list_in:
        if int(line[param]) > current_max:
            fips = line[0]
            current_max = int(line[param])
    with open(csvTitleHardCode) as f:
        for line in f:
            line = line.rstrip().split('","')
            line[0] = line[0].replace('"', '')
            line[1] = line[1].replace('"', '')
            if fips == str(line[0]):
                fips_name = line[1]
                return fips_name, current_max
    return '', current_max


# function definition for calculating unique and distinct values in a list
# this function returns two values, first the # of unique values and then # of distinct
# NOTE: this function will crash if focus parameter is not one of the specified strings
def count_unique_and_distinct(focus, list_in):
    d = {}
    if focus == 'wages':
        param = 10
    elif focus == 'estabs':
        param = 8
    elif focus == 'emplvl':
        param = 9
    for line in list_in:
        if line[param] in d:
            d[line[param]] = False
        else:
            d[line[param]] = True
    return len([i for i in d if d[i]]), len(d)


# function definition to sort the list depending on what we are sorting
# this function returns the sorted list according to the focus string specified
# NOTE: this function will crash if focus parameter is not one of the specified strings
def sort_my_list(focus, list_in):
    if focus == 'wages':
        def getElements1_0(lst):
            return lst[10], -int(lst[0])
    elif focus == 'estabs':
        def getElements1_0(lst):
            return lst[8], -int(lst[0])
    elif focus == 'emplvl':
        def getElements1_0(lst):
            return lst[9], -int(lst[0])
    return sorted(list_in, key=getElements1_0)


# function definition to search for what rank something is. The list passed in must be sorted.
# this function returns the value for the rank choosing the end of the list to be 1st and going in reverse order
def find_rank(countyName, list_in):
    with open(csvTitleHardCode) as f:
        for line in f:
            line = line.rstrip().split('","')
            line[0] = line[0].replace('"', '')
            line[1] = line[1].replace('"', '')
            if countyName == line[1]:
                for rank, listing in enumerate(list_in):
                    if line[0] == listing[0]:
                        return len(list_in) - rank


# function definition to create the list of top values and there FIPS area names
# this function returns the top list of how ever many values specified
# NOTE: this function will crash if focus parameter is not one of the specified strings
def top_list(focus, howMany, list_in):
    if focus == 'wages':
        param = 10
    elif focus == 'estabs':
        param = 8
    elif focus == 'emplvl':
        param = 9
    the_top = []
    for l in reversed(list_in[-howMany:]):
        with open(csvTitleHardCode) as f:
            for line in f:
                line = line.rstrip().split('","')
                line[0] = line[0].replace('"', '')
                line[1] = line[1].replace('"', '')
                if l[0] == line[0]:
                    the_top.append((line[1], l[param]))
    return the_top


# we start by going into the directory specified in the cmd line
os.chdir(sys.argv[1])

# this creates our new data sets for all and for soft
all_industries = new_dataset('10', '0')
soft_industry = new_dataset('5112', '5')

# we then do all the value assignments by calling our functions defined earlier for all_industries
rpt.all.count = len(all_industries)

all_wage_sort = sort_my_list('wages', all_industries)
rpt.all.total_pay = calc_totals('wages', all_industries)
rpt.all.unique_pay, rpt.all.distinct_pay = count_unique_and_distinct('wages', all_industries)
rpt.all.cache_co_pay_rank = find_rank('Cache County, Utah', all_wage_sort)
rpt.all.top_annual_wages = top_list("wages", 5, all_wage_sort)

all_estab_sort = sort_my_list('estabs', all_industries)
rpt.all.total_estab = calc_totals('estabs', all_industries)
rpt.all.unique_estab, rpt.all.distinct_estab = count_unique_and_distinct('estabs', all_industries)
rpt.all.cache_co_estab_rank = find_rank('Cache County, Utah', all_estab_sort)
rpt.all.top_annual_estab = top_list("estabs", 5, all_estab_sort)

all_empl_sort = sort_my_list('emplvl', all_industries)
rpt.all.total_empl = calc_totals('emplvl', all_industries)
rpt.all.unique_empl, rpt.all.distinct_empl = count_unique_and_distinct('emplvl', all_industries)
rpt.all.cache_co_empl_rank = find_rank('Cache County, Utah', all_empl_sort)
rpt.all.top_annual_avg_emplvl = top_list("emplvl", 5, all_empl_sort)

rpt.all.per_capita_avg_wage = rpt.all.total_pay/rpt.all.total_empl


# and we do the same value assignments with the soft_industry
rpt.soft.count = len(soft_industry)

soft_wage_sort = sort_my_list('wages', soft_industry)
rpt.soft.total_pay = calc_totals('wages', soft_industry)
rpt.soft.unique_pay, rpt.soft.distinct_pay = count_unique_and_distinct('wages', soft_industry)
rpt.soft.cache_co_pay_rank = find_rank('Cache County, Utah', sort_my_list('wages', soft_industry))
rpt.soft.top_annual_wages = top_list("wages", 5, soft_wage_sort)

soft_estab_sort = sort_my_list('estabs', soft_industry)
rpt.soft.total_estab = calc_totals('estabs', soft_industry)
rpt.soft.unique_estab, rpt.soft.distinct_estab = count_unique_and_distinct('estabs', soft_industry)
rpt.soft.cache_co_estab_rank = find_rank('Cache County, Utah', sort_my_list('estabs', soft_industry))
rpt.soft.top_annual_estab = top_list("estabs", 5, soft_estab_sort)

soft_empl_sort = sort_my_list('emplvl', soft_industry)
rpt.soft.total_empl = calc_totals('emplvl', soft_industry)
rpt.soft.unique_empl, rpt.soft.distinct_empl = count_unique_and_distinct('emplvl', soft_industry)
rpt.soft.cache_co_empl_rank = find_rank('Cache County, Utah', sort_my_list('emplvl', soft_industry))
rpt.soft.top_annual_avg_emplvl = top_list("emplvl", 5, soft_empl_sort)

rpt.soft.per_capita_avg_wage = rpt.soft.total_pay/rpt.soft.total_empl

# By the time you submit your work to me, this should be the *only* output your
# entire program produces.
print(rpt)
